package snake;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import static snake.GameInit.*;

public class Game implements Runnable{
    private Display display;
    private Thread thread;

    private BufferStrategy bs;
    private Graphics g;

    //States
    private State gameState;
    private State menuState;

    //Input
    private KeyManager keyManager;

    private boolean running = false;

    public Game(){
        keyManager = new KeyManager();
    }
    private void init(){
        display = new Display(GAME_TITLE, new Dimension(GAME_WIDTH, GAME_HEIGHT));
        display.getFrame().addKeyListener(keyManager);

        gameState = new GameState(this);
        menuState = new MenuState(this);
        State.setState(gameState);
    }
    private void update(){
        keyManager.update();

        if(State.getState() != null)
            State.getState().update();
    }
    private void render(){
        bs = display.getCanvas().getBufferStrategy();
        if(bs == null){
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        g = bs.getDrawGraphics();
        g.clearRect(0,0,GAME_WIDTH, GAME_HEIGHT);
        //desenhar
        if(State.getState() != null)
            State.getState().render(g);

        //acaba de desenhar
        bs.show();
        g.dispose();
    }
    public void run(){
        init();
        gameLoop(FRAME_RATE);
        stop();
    }
    public KeyManager getKeyManager(){
        return keyManager;
    }
    public synchronized void start(){
        if(running)
            return; //se já estiver rodando, o método start não faz nada
        running = true;
        thread = new Thread(this);
        thread.start(); //chama o método run()
    }
    public synchronized void stop(){
        if(!running)
            return; //se não tiver nada pra parar, o método stop não faz nada;
        try {
            thread.join();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }
    private void gameLoop(int fps){
        double timePerUpdate = 1e9/fps;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;
        int ticks = 0;

        while(running){
            now = System.nanoTime();
            delta+= (now-lastTime)/timePerUpdate;
            timer+= now-lastTime;
            lastTime = now;

            if(delta >= 1) {
                update();
                render();
                ticks++;
                delta--;
            }

            if(timer >= 1e9){
                display.changeWindowTitle(GAME_TITLE + " - FPS: " + ticks);
                ticks = 0;
                timer = 0;
            }
        }
    }
}
