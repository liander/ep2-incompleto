package snake;

import java.awt.*;

public abstract class Entity {
    protected float x,y;
    protected int width, height;
    protected Color color;
    protected Game game;

    public Entity(int size){
        this.width = size;
        this.height = size;
    }
    public Entity(int x, int y, int size){
        this.width = size;
        this.height = size;
    }
    public Entity(int x, int y, int size, Game game, Color color){
        this.x = x;
        this.y = y;
        this.width = size;
        this.height = size;
        this.game = game;
        this.color = color;
    }
    public abstract void update();
    public abstract void render(Graphics g);

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Rectangle getBound(){
        return new Rectangle((int)x,(int)y,width,height);
    }


    public boolean isCollision(Entity o){
        if(o == this) return false;
        return getBound().intersects(o.getBound());
    }
}
