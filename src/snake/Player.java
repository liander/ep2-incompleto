package snake;

import java.awt.*;

import static snake.GameInit.SNAKE_PART_SIZE;

public class Player extends Entity{
    private Color color;
    public int score = 0;
    public int size;

    private int dy, dx;
    public Player(Game game, int size, Color color){
        super(size);
        this.size = size;
    }
    public Player(Game game, Dimension spawn, int size, Color color){
        super(spawn.width, spawn.height ,size, game, color);
        this.size = size;
        this.color = color;
    }

    @Override
    public void update() {

    }

    @Override
    public void render(Graphics g) {
        g.setColor(color);
        g.fillRect((int)x +1, (int)y +1, size-2, size-2 );
    }

    public void move(int dx, int dy){
        x+=dx;
        y+=dy;
    }

}
