package snake;

import javax.swing.*;
import java.awt.*;

public class Display {

    private JFrame frame;
    private Canvas canvas;

    private String title;
    private Dimension gameWindowDimension;

    public Display(String title, Dimension windowSize){ //composição
        this.title = title;
        this.gameWindowDimension = windowSize;

        createWindow();
        setCanvas();
    }

    private void createWindow(){
        frame = new JFrame(title);
        frame.setSize(gameWindowDimension);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    private void setCanvas(){
        canvas = new Canvas();
        canvas.setPreferredSize(gameWindowDimension);
        canvas.setMaximumSize(gameWindowDimension);
        canvas.setMinimumSize(gameWindowDimension);
        canvas.setFocusable(false);

        frame.add(canvas);
        frame.pack();
    }

    public Canvas getCanvas(){
        return canvas;
    }

    public void changeWindowTitle(String newTitle){
        frame.setTitle(newTitle);
    }

    public JFrame getFrame(){
        return frame;
    }
}
