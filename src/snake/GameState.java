package snake;

import java.awt.*;
import java.util.ArrayList;

import static snake.GameInit.*;

public class GameState extends State {
    private Player head;
    private Fruit fruit;
    private ArrayList<Player> body;
    private int dy, dx;
    public GameState(Game game){
        super(game);
        body = new ArrayList<Player>();
        head = new Player(game, GAME_CENTER,SNAKE_PART_SIZE, Color.yellow);
        body.add(head);

        for(int i = 1; i<10;i++){
            Player p = new Player(game, SNAKE_PART_SIZE, Color.yellow);
            p.setPosition((int)head.getX() + (i*SNAKE_PART_SIZE), (int)head.getY());
            body.add(p);
        }
        fruit = new Fruit(30, 30, 30, game, Color.RED);
    }

    public void update(){
        System.out.println(head.getX() + " , " + head.getY());
        if(game.getKeyManager().up && dy == 0){
            dy = -SNAKE_PART_SIZE;
            dx = 0;
        }
        if(game.getKeyManager().down) {
            dy = SNAKE_PART_SIZE;
            dx = 0;
        }
        if(game.getKeyManager().left) {
            dx = -SNAKE_PART_SIZE;
            dy = 0;
        }
        if(game.getKeyManager().right) {
            dx = SNAKE_PART_SIZE;
            dy = 0;
        }
        if(dx!=0 || dy!=0) {
            for (int i = body.size() - 1; i > 0; i--) {
                body.get(i).setPosition(
                        (int) body.get(i - 1).getX(),
                        (int) body.get(i - 1).getY()
                );
            }
            head.move(dx, dy);
        }
        if(noClip){
            if (head.getX() < 0) head.setX(GAME_WIDTH - SNAKE_PART_SIZE);
            if (head.getY() < 0) head.setY(GAME_HEIGHT - SNAKE_PART_SIZE);
            if (head.getX() >= GAME_WIDTH) head.setX(0);
            if (head.getY() >= GAME_HEIGHT) head.setY(0);
        }else{
            //Colisao aqui
        }
        if(fruit.isCollision(head)) fruit.placeFruit();
    }

    public void render(Graphics g){
        for(Player p: body){
            p.render(g);
        }
        fruit.render(g);
    }
}
