package snake;

import java.awt.*;

import static snake.GameInit.*;

public class Fruit extends Entity implements Runnable {
    private Thread thread;

    public Fruit(int size) {
        super(size);
    }

    public Fruit(int x, int y, int size) {
        super(x, y, size);
    }

    public Fruit(int x, int y, int size, Game game, Color color) {
        super(x, y, size, game, color);
    }

    @Override
    public void update() {

    }

    @Override
    public void render(Graphics g) {
        g.setColor(color);
        g.fillRect((int)x +1, (int)y +1, SNAKE_PART_SIZE-2, SNAKE_PART_SIZE-2 );
    }
    public void placeFruit(){
        int x = (int)(Math.random() * (GAME_WIDTH-SNAKE_PART_SIZE));
        int y = (int)(Math.random() * (GAME_HEIGHT-SNAKE_PART_SIZE));
        y = y - (y%SNAKE_PART_SIZE);
        x = x - (x%SNAKE_PART_SIZE);
        this.setPosition(x,y);
    }
    public void run(){

    }
}
