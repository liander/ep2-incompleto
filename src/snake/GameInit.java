package snake;

import java.awt.*;

public class GameInit {
    public static final String GAME_TITLE = "Cobrinha do EP2";
    public static final int GAME_WIDTH = 720;
    public static final int GAME_HEIGHT = 480;
    public static final int SNAKE_PART_SIZE = 15;
    public static final int FRUIT_SIZE = 50;
    public static final int FRAME_RATE = 20;
    public static int appleType = 0;
    public static int snakeType = 0;
    public static boolean noClip = true;
    public static final Dimension GAME_CENTER = new Dimension((GAME_WIDTH/2)-(SNAKE_PART_SIZE/2), (GAME_HEIGHT/2)-(SNAKE_PART_SIZE/2));

    public static void main(String[] args){
        System.out.println("Jogo iniciado!\n");
        Game game = new Game();
        game.start();
    }

}
